# SQL
```sql
select
date(`date`) as `day`,
coalesce(sum(score > 0), '-') as `num_pos_scores`,
coalesce(sum(score < 0), '-') as `num_neg_scores`
from assessment
where date(`date`) between '2011-03-11' AND '2011-04-30'
group by `day`;
```

```sql
select date(`date`) as `day`
from
assessment
where
score > 0
having `day` between '2011-03-11' AND '2019-08-18'
```

# Backend PHP

What’s the difference between the include() and require()functions?
- include : produce a warning on failure
- require  : produce fatal error on failure  and halt the code execution

How can we get the IP address of the client?
- $_SERVER['CLIENT_IP']
- $_SERVER['HTTP_X_FORWARDED_FOR']
- $_SERVER['REMOTE_ADDR']

What’s the difference between unset() and unlink()
- unlink() is for removing file
- unset() is for remove variable, array or class property

What are the main error types in PHP and how do they differ?
- syntax error : error/missing something (semicolon, closing bracket, etc) in code. It will halt the execution
- fatal error : no error on code syntax, but failed due to some error code flow failure (exception, undefined method, etc). This will also halt the code execution
- warning : got an error, will not halt the code execution but might produce some bug because of it
- notice : lowest error severity. will not halt the code execution

What is the difference between GET and POST?
- GET : for accessing data or page. has query params on url
- POST : for creating new data. has request body

How can you enable error reporting in PHP?
- Through error_reporting directive in php.ini
- Through error_reporting() function

What are Traits?
- For appending some methods in a class
- A class can use more than one traits

How we can get the number of elements in an array?
- count it using count()

What are the __construct() and __destruct() methods in a PHP class?
- __construct() : all code inside this method will run when class instantianted
- __destruct() : an opposite of __construct, it will run on the latest order

Dragonball ...
```php
<?php

class DragonBall
{
    private $ballCount = 0;
    const MAX_BALL = 7

    public function iFoundBall()
    {
        $this->ballCount++;

        if ($this->ballCount == static::MAX_BALL) {
            $this->askAWish();
            $this->reset();
        }
    }

    private function askAWish()
    {
        echo 'You can ask your wish .. ';
    }

    private function reset()
    {
        $this->ballCount = 0;
    }
}
```

Explain what is MVC in PHP
- Common software architecture for separating business logic and presentation.
Model : Contains all core business logic that related with data
View : Contains all presentation logic
Controller : Responsible for handle user requests/inputs to model and view

## TODO API
For TODO api please go to : https://gitlab.com/aaw.personal.1/qlue-backend-test/tree/master/todo and follow the README instructions
The API documentation can be accessed through : https://documenter.getpostman.com/view/340222/SVfKyr6D?version=latest
