<?php

class DragonBall
{
    private $ballCount = 0;
    const MAX_BALL = 7

    public function iFoundBall()
    {
        $this->ballCount++;

        if ($this->ballCount == static::MAX_BALL) {
            $this->askAWish();
            $this->reset();
        }
    }

    private function askAWish()
    {
        echo 'You can ask your wish .. ';
    }

    private function reset()
    {
        $this->ballCount = 0;
    }
}
