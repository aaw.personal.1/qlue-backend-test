<?php
declare(strict_types=1);

use App\Domain\Todo\TodoRepository;
use App\Domain\Account\AccountRepository;
use App\Infrastructure\Persistence\Todo\JsonFileTodoRepository;
use App\Infrastructure\Persistence\Account\JsonFileAccountRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    // Here we map our UserRepository interface to its in memory implementation
    $containerBuilder->addDefinitions([
        TodoRepository::class => \DI\autowire(JsonFileTodoRepository::class),
        AccountRepository::class => \DI\autowire(JsonFileAccountRepository::class),
    ]);
};
