<?php
declare(strict_types=1);

use Slim\App;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;
use App\Application\Middleware\AuthorizationBearerMiddleware;

// Actions
use App\Application\Actions\Account\RegisterAction;
use App\Application\Actions\Account\LoginAction;
use App\Application\Actions\Account\LogoutAction;
use App\Application\Actions\Todo\ListTodoAction;
use App\Application\Actions\Todo\ViewTodoAction;
use App\Application\Actions\Todo\PostTodoAction;
use App\Application\Actions\Todo\DeleteTodoAction;
use App\Application\Actions\Todo\UpdateTodoAction;

return function (App $app) {
    $container = $app->getContainer();

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write('Hello world');
        return $response;
    });

    $app->group('/api/v1', function(Group $app) use ($container) {
        // account
        $app->post('/login', LoginAction::class);
        $app->get('/logout', LogoutAction::class);
        $app->post('/register', RegisterAction::class);

        // todos
        $app->group('/todo', function (Group $app) {
            $app->post('', PostTodoAction::class);
            $app->get('', ListTodoAction::class);
            $app->get('/{id}', ViewTodoAction::class);
            $app->delete('/{id}', DeleteTodoAction::class);
            $app->put('/{id}', UpdateTodoAction::class);
        })->add(AuthorizationBearerMiddleware::class);
    });
};
