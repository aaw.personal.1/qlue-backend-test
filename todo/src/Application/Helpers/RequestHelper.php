<?php
declare(strict_types=1);

namespace App\Application\Helpers;

class RequestHelper
{
    /**
     * Get bearer token from headers
     * @return array $headers
     * @return string
     */
    public static function getBearerToken(array $headers): string
    {
        $headers = $headers['Authorization'][0];
        list($bearer, $token) = explode(' ', $headers);

        return $token;
    }
}
