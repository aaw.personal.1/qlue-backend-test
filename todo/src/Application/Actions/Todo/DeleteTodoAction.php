<?php
declare(strict_types=1);

namespace App\Application\Actions\Todo;

use Psr\Http\Message\ResponseInterface as Response;

class DeleteTodoAction extends TodoAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $todoId = $this->resolveArg('id');
        $this->todoRepository->delete($todoId);

        return $this->respondWithData(NULL, 204);
    }
}
