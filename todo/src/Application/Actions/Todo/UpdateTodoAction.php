<?php
declare(strict_types=1);

namespace App\Application\Actions\Todo;

use Psr\Http\Message\ResponseInterface as Response;

class UpdateTodoAction extends TodoAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $todoId = $this->resolveArg('id');
        $formData = $this->getFormData();
        $updated = $this->todoRepository->update($todoId, $formData->body);

        return $this->respondWithData($updated, 200);
    }
}
