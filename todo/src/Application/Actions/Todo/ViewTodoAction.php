<?php
declare(strict_types=1);

namespace App\Application\Actions\Todo;

use Psr\Http\Message\ResponseInterface as Response;

class ViewTodoAction extends TodoAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $todoId = $this->resolveArg('id');
        $todo = $this->todoRepository->getById($todoId);

        return $this->respondWithData($todo, 200);
    }
}
