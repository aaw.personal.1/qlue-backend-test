<?php
declare(strict_types=1);

namespace App\Application\Actions;

use \Firebase\JWT\JWT;

trait HasToken
{
    /**
     * @var string
     */
    private $tokenDir = '../storage/tokens';

    /**
     * @var string
     *
     * TODO
     * Store in config instead
     */
    private $signedKey = 'secured-jwt-key';

    /**
     * Check if token exists in storage
     * @return bool
     */
    protected function tokenExists($token): bool
    {
        // TODO
        // Also check expiry
        return file_exists($this->tokenDir . '/' . $token);
    }

    /**
     * Generate token
     * @return string
     */
    protected function getToken($sub): string
    {
        $token = array(
            'sub' => $sub,
            'iss' => 'http://todo',
            'aud' => 'http://todo',
            'iat' => time(),
            'exp' => time() + 3600,
        );

        return JWT::encode($token, $this->signedKey);
    }

    /**
     * Store token to storage
     * @return void
     */
    protected function storeToken($token): void
    {
        touch('../storage/tokens/' . $token);
    }

    /**
     * Remove token from storage
     * @return void
     */
    protected function removeToken($token): void
    {
        unlink('../storage/tokens/' . $token);
    }

    /**
     * Get bearer token from headers
     * @return array $headers
     * @return string|null
     */
    protected function getBearerToken(array $headers)
    {
        if (!isset($headers['Authorization'][0])) return NULL;

        $headers = $headers['Authorization'][0];
        list($bearer, $token) = explode(' ', $headers);

        return $token;
    }
}
