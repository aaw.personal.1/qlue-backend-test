<?php
declare(strict_types=1);

namespace App\Application\Actions\Account;

use Psr\Http\Message\ResponseInterface as Response;

class RegisterAction extends AccountAction
{
    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $formData = $this->getFormData();
        $registered = $this->accountRepository->register(
            $formData->username,
            $formData->password
        );

        return $this->respondWithData($registered, 201);
    }
}
