<?php
declare(strict_types=1);

namespace App\Application\Actions\Account;

use Psr\Http\Message\ResponseInterface as Response;
use App\Application\Actions\HasToken;

class LoginAction extends AccountAction
{
    use HasToken;

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $formData = $this->getFormData();
        $validUser = $this->accountRepository->validUser(
            $formData->username,
            $formData->password
        );
        if (!$validUser) throw new \Slim\Exception\HttpUnauthorizedException($this->request, 'Invalid credentials');

        $token = $this->getToken($formData->username);
        $payload = $token;
        $this->storeToken($token);

        return $this->respondWithData($token, 200);
    }
}
