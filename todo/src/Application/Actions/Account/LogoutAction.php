<?php
declare(strict_types=1);

namespace App\Application\Actions\Account;

use Psr\Http\Message\ResponseInterface as Response;
use App\Application\Actions\HasToken;

class LogoutAction extends AccountAction
{
    use HasToken;

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $bearerToken = $this->getBearerToken($this->request->getHeaders());
        if($this->tokenExists($bearerToken)) {
            $this->removeToken($bearerToken);
            return $this->respondWithData('Token removed');
        }

        throw new \Slim\Exception\HttpUnauthorizedException($this->request, 'Invalid or expired token');
    }
}
