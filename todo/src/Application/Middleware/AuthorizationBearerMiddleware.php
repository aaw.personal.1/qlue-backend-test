<?php
declare(strict_types=1);

namespace App\Application\Middleware;

use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Http\Server\MiddlewareInterface as Middleware;
use Psr\Http\Server\RequestHandlerInterface as RequestHandler;
use App\Application\Helpers\RequestHelper;
use App\Application\Actions;
use App\Application\Actions\HasToken;

class AuthorizationBearerMiddleware implements Middleware
{
    use HasToken;

    /**
     * @param  \Psr\Http\Message\ServerRequestInterface $request  PSR7 request
     * @param  \Psr\Http\Message\ResponseInterface      $response PSR7 response
     * @param  callable                                 $next     Next middleware
     *
     * @return \Psr\Http\Message\ResponseInterface
     */
    public function process(Request $request, RequestHandler $handler): Response
    {
        $token = $this->getBearerToken($request->getHeaders());
        if(!empty($token) && $this->tokenExists($token)) {
            return $handler->handle($request);
        }

        throw new \Slim\Exception\HttpUnauthorizedException($request, 'Token is missing or expired');
    }
}
