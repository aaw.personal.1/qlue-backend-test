<?php
declare(strict_types=1);

namespace App\Domain\Todo;

use JsonSerializable;

class Todo implements JsonSerializable
{
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $body;

    /**
     * @param string    $id
     * @param string    $body
     */
    public function __construct(?string $id, string $body)
    {
        $this->id = $id;
        $this->body = $body;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'id' => $this->id,
            'body' => $this->body,
        ];
    }
}
