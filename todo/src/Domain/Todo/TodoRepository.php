<?php
declare(strict_types=1);

namespace App\Domain\Todo;

interface TodoRepository
{
    /**
     * @return Todo
     */
    public function create(string $body): Todo;

    /**
     * @param string $id
     * @param string $password
     * @return Todo
     */
    public function update(string $id, string $body): Todo;

    /**
     * @param string $id
     * @return Todo
     */
    public function getById(string $id): Todo;

    /**
     * @param string $id
     * @return bool
     */
    public function delete(string $id): bool;

    /**
     * @return []Todo
     */
    public function getAll(): array;
}
