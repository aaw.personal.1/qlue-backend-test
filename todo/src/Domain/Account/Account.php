<?php
declare(strict_types=1);

namespace App\Domain\Account;

use JsonSerializable;

class Account implements JsonSerializable
{
    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @param string    $username
     * @param string    $password
     */
    public function __construct(string $username, string $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return strtolower($this->username);
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return [
            'username' => $this->username,
        ];
    }
}
