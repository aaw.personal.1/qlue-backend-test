<?php
declare(strict_types=1);

namespace App\Domain\Account;

interface AccountRepository
{
    /**
     * @return Account
     */
    public function register(string $username, string $password): Account;

    /**
     * @param string $username
     * @param string $password
     * @return bool
     */
    public function validUser(string $username, string $password): bool;
}
