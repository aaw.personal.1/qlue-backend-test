<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Account;

use App\Domain\Account\Account;
use App\Domain\Account\AccountRepository;
use App\Infrastructure\Persistence\JsonFileStorageAbstract;

class JsonFileAccountRepository extends JsonFileStorageAbstract implements AccountRepository
{
    /**
     * {@inheritdoc}
     */
    protected $storageName = 'account';

    /**
     * {@inheritdoc}
     */
    public function register(string $username, string $password): Account 
    {
        $account = $this->write([$username => $password]);
        if ($account) return new Account($username, $password);

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function validUser(string $username, string $password): bool
    {
        $accounts = $this->read();
        if (!isset($accounts[$username])) return false;

        return $accounts[$username] == $password;
    }
}
