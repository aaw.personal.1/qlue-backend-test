<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence\Todo;

use App\Domain\Todo\Todo;
use App\Domain\Todo\TodoRepository;
use App\Domain\Todo\TodoNotFoundException;
use App\Infrastructure\Persistence\JsonFileStorageAbstract;
use App\Infrastructure\Persistence\HasUniqueId;

class JsonFileTodoRepository extends JsonFileStorageAbstract implements TodoRepository
{
    use HasUniqueId;

    /**
     * {@inheritdoc}
     */
    protected $storageName = 'todo';

    /**
     * {@inheritdoc}
     */
    public function create(string $body): Todo
    {
        $id = $this->uniqueId();
        $todo = $this->write([$id => $body]);
        if ($todo) return new Todo($id, $body);

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function update(string $id, string $body): Todo
    {
        $todos = $this->read();
        if (!isset($todos[$id])) throw new TodoNotFoundException;

        $todos[$id] = $body;
        $this->replaceAll($todos);

        return $this->getById($id);
    }

    /**
     * {@inheritdoc}
     */
    public function delete(string $id): bool
    {
        $todos = $this->read();
        if (!isset($todos[$id])) throw new TodoNotFoundException;

        unset($todos[$id]);
        return $this->replaceAll($todos);
    }

    /**
     * {@inheritdoc}
     */
    public function getAll(): array
    {
        $todos = $this->read();

        if (count($todos) > 0) {
            return array_map(function($key, $value) {
                return new Todo($key, $value);
            }, array_keys($todos), $todos);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getById(string $id): Todo
    {
        $todos = $this->read();
        if (!isset($todos[$id])) throw new TodoNotFoundException;

        return new Todo($id, $todos[$id]);
    }
}
