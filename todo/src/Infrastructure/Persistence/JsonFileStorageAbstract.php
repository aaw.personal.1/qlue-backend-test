<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence;

abstract class JsonFileStorageAbstract
{
    /**
     * Filename without extension
     *
     * @var string
     */
    protected $storageName;

    /**
     * Directory path
     *
     * @var string
     */
    protected $dir = '../storage/db';

    /**
     * @var Read all contents from file
     * @return array
     */
    protected function read(): array
    {
        if (!$this->fileExists()) $this->touchFile();

        $handle = fopen($this->getFilename(), 'r');
        if (filesize($this->getFilename()) < 1) return [];

        $data = fread($handle, filesize($this->getFilename()));

        return $this->unserialize($data);
    }

    /**
     * @var Write all contents to a file
     * @return boolean
     */
    protected function write(array $content): bool
    {
        $currentContent = $this->read();
        $handle = fopen($this->getFilename(), 'w') or die('Cannot open file:  '.$this->getFilename());
        $content = json_encode(array_merge($currentContent, $content));

        return (bool) fwrite($handle, $content);
    }

    protected function replaceAll(array $content): bool
    {
        $handle = fopen($this->getFilename(), 'w') or die('Cannot open file:  '.$this->getFilename());
        $content = json_encode($content);

        return (bool) fwrite($handle, $content);
    }

    protected function fileExists()
    {
        return file_exists($this->getFilename());
    }

    private function touchFile()
    {
        touch($this->getFilename());
    }

    private function unserialize($content)
    {
        return json_decode($content, true);
    }

    private function getFilename()
    {
        if (empty($this->storageName)) throw new \Exception('Empty storageName');

        return $this->dir . '/' . $this->storageName . '.json';
    }
}
