<?php
declare(strict_types=1);

namespace App\Infrastructure\Persistence;

trait HasUniqueId
{
    public function uniqueId()
    {
        return bin2hex(random_bytes(16));
    }
}
